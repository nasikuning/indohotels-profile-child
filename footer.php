  <footer class="main-footer">
  <div class="footer-link">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-md-3">
        <?php karisma_nav_footer(); ?>
        </div>
        <div class="col-xs-6 col-md-3">
          <div class="location">
            <p>LOCATION:</p>
            <p>Jl. Kapas No. 1, Caturtunggal, Depok, Sleman, Daerah Istimewa Yogyakarta 55281</p>
          </div>
        </div>
        <div class="col-xs-6 col-md-3">
          <div class="location">
            <p>CONTACT:</p>
            <p>
              <span>info.indohotels@gmail.com</span><br />
              <span>0274 453 888</span>
            </p>
          </div>
        </div>
        <div class="col-xs-6 col-md-3">
          <div class="info-socmed">
            <ul class="clearfix">
              <li><a href="" class="icn-facebook"><i class="fa fa-facebook" aria-hidden="true"></i> indohotels.id</a></li>
              <li><a href="" class="icn-twitter"><i class="fa fa-twitter" aria-hidden="true"></i> indohotels.id</a></li>
              <li><a href="" class="icn-instagram"><i class="fa fa-instagram" aria-hidden="true"></i> indohotels.id</a></li>
            </ul>
          </div>
        </div>
      </div><!-- /.row -->

    </div><!-- end  .container -->
  </div><!-- end .footer-content -->
  <div class="footer-info">
    <div class="container">
      <div class="verticent">
        <p class="verticent-inner">Jl. Laksda Adisucipto Km 6.5 Seturan Yogyakarta |  P: +62 274 493 2000 |  F: +62 274 493 2222</p>
      </div>
    </div><!-- end .container -->
  </div><!-- end .footer-info -->
</footer>


</body>
<?php wp_footer(); ?>

</html>