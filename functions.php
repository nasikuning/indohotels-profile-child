<?php

define('PROFILE_CHILD', get_stylesheet_directory_uri());
define('child_dir', get_stylesheet_directory());

function my_theme_enqueue_styles() {

    $parent_style = 'karisma-style';
    wp_enqueue_script('main-child', get_stylesheet_directory_uri() . '/asset/js/global.js', array(), false, true );

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
	);
	if((is_page( array( 'user', 'dashboard', 'member' , 'service', 'invoice', 'development') ) ) && is_user_logged_in()){
		wp_enqueue_style( 'custom-child-style',
			get_stylesheet_directory_uri() . '/asset/css/custom.css',
			array( $parent_style ),
			wp_get_theme()->get('Version')
		);
	}

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 99 );

function krs_member_nav()
{
	wp_nav_menu(
		array(
			'theme_location'  => 'member-menu',
			'menu'            => 'member-menu',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'member-navbar-collapse-1',
			'menu_class'        => 'nav navbar-nav',
			'fallback_cb'       => '',
			'walker'            => ''
			)
	);
}
function krs_member_user_nav()
{
	wp_nav_menu(
		array(
			'theme_location'  => 'member-user-menu',
			'menu'            => 'member-user-menu',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'member-navbar-collapse-1',
			'menu_class'        => 'nav navbar-nav navbar-right',
			'fallback_cb'       => '',
			'walker'            => ''
			)
	);
}
function krs_member_nav_mobile()
{
	wp_nav_menu(
		array(
			'theme_location'  => 'member-menu',
			'menu'            => 'member-menu',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'member-navbar-collapse-1',
			'menu_class'        => 'monav',
			'fallback_cb'       => '',
			'walker'            => ''
			)
	);
}
function krs_member_user_nav_mobile()
{
	wp_nav_menu(
		array(
			'theme_location'  => 'member-user-menu',
			'menu'            => 'member-user-menu',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'member-navbar-collapse-1',
			'menu_class'        => 'monav',
			'fallback_cb'       => '',
			'walker'            => ''
			)
	);
}

function register_karisma_menu()
{
	register_nav_menus(array( // Using array to specify more menus if needed
		'header-menu' => __('Header Menu', karisma_text_domain), 
		'footer-menu' => __('Footer Menu', karisma_text_domain), 
		'member-menu' => __('Member Menu', karisma_text_domain),
		'member-user-menu' => __('Member User Menu', karisma_text_domain)
		));
}







/**
 * Redirect to the custom login page
 */
function cubiq_login_init () {

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';

	if ( isset( $_POST['wp-submit'] ) ) {

		$action = 'post-data';

	} else if ( isset( $_GET['reauth'] ) ) {

		$action = 'reauth';

	}

	// redirect to change password form
	if ( $action == 'rp' || $action == 'resetpass' ) {
		if( isset($_GET['key']) && isset($_GET['login']) ) {
            $rp_path = wp_unslash(home_url('/login/')); //fix reset password
			$rp_cookie	= 'wp-resetpass-' . COOKIEHASH;
			$value = sprintf( '%s:%s', wp_unslash( $_GET['login'] ), wp_unslash( $_GET['key'] ) );
			setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
		}
		
		// wp_redirect( home_url('/login/?action=resetpass') );
		wp_redirect( home_url('/login/?action=resetpass&key=' . $_GET['key']) ); //Fix Create Password for new user

		exit;
	}

	// redirect from wrong key when resetting password
	if ( $action == 'lostpassword' && isset($_GET['error']) && ( $_GET['error'] == 'expiredkey' || $_GET['error'] == 'invalidkey' ) ) {
		wp_redirect( home_url( '/login/?action=forgot&failed=wrongkey' ) );
		exit;
	}

	if (
		$action == 'post-data'		||			// don't mess with POST requests
		$action == 'reauth'			||			// need to reauthorize
		$action == 'logout'						// user is logging out
	) {
		return;
	}

	wp_redirect( home_url( '/login/' ) );
	exit;
}
add_action('login_init', 'cubiq_login_init');


/**
 * Redirect logged in users to the right page
 */
function cubiq_template_redirect () {
	if ( is_page( 'login' ) && is_user_logged_in() ) {
		wp_redirect( home_url( '/member/' ) );
		exit();
	}

	if ( (is_page( 'user' ) || is_page( 'member' ) || is_page( 'service' ) || is_page( 'invoice' ) ||  is_page( 'development' )) && !is_user_logged_in() ) {
		wp_redirect( home_url( '/login/' ) );
		exit();
	}
}
add_action( 'template_redirect', 'cubiq_template_redirect' );


/**
 * Prevent subscribers to access the admin
 */
function cubiq_admin_init () {

	if ( current_user_can( 'subscriber' ) && !defined( 'DOING_AJAX' ) ) {

		wp_redirect( home_url('/member/') );

		exit;
	}

}
add_action( 'admin_init', 'cubiq_admin_init' );


/**
 * Registration page redirect
 */
function cubiq_registration_redirect ($errors, $sanitized_user_login, $user_email) {

	
	// don't lose your time with spammers, redirect them to a success page
	if ( !isset($_POST['confirm_email']) || $_POST['confirm_email'] !== '' ) {

		wp_redirect( home_url('/login/') . '?action=register&success=1' );
		
		exit;

	}

	if ( !empty( $errors->errors) ) {

		if ( isset( $errors->errors['username_exists'] ) ) {

			wp_redirect( home_url('/login/') . '?action=register&failed=username_exists' );

		} else if ( isset( $errors->errors['email_exists'] ) ) {

			wp_redirect( home_url('/login/') . '?action=register&failed=email_exists' );

		} else if ( isset( $errors->errors['invalid_username'] ) ) {

			wp_redirect( home_url('/login/') . '?action=register&failed=invalid_username' );
			
		} else if ( isset( $errors->errors['invalid_email'] ) ) {
+
+			wp_redirect( home_url('/login/') . '?action=register&failed=invalid_email' );

		} else if ( isset( $errors->errors['empty_username'] ) || isset( $errors->errors['empty_email'] ) ) {

			wp_redirect( home_url('/login/') . '?action=register&failed=empty' );

		} else {

			wp_redirect( home_url('/login/') . '?action=register&failed=generic' );

		}

		exit;
	}

	return $errors;

}
add_filter('registration_errors', 'cubiq_registration_redirect', 10, 3);


/**
 * Login page redirect
 */
function cubiq_login_redirect ($redirect_to, $url, $user) {

	if ( !isset($user->errors) ) {
		return $redirect_to;
	}

	wp_redirect( home_url('/login/') . '?action=login&failed=1');
	exit;

}
add_filter('login_redirect', 'cubiq_login_redirect', 10, 3);


/**
 * Password reset redirect
 */
function cubiq_reset_password () {
	$user_data = '';

	if ( !empty( $_POST['user_login'] ) ) {
		if ( strpos( $_POST['user_login'], '@' ) ) {
			$user_data = get_user_by( 'email', trim($_POST['user_login']) );
		} else {
			$user_data = get_user_by( 'login', trim($_POST['user_login']) );
		}
	}

	if ( empty($user_data) ) {
		wp_redirect( home_url('/login/') . '?action=forgot&failed=1' );
		exit;
	}
}
add_action( 'lostpassword_post', 'cubiq_reset_password');


/**
 * Validate password reset
 */
function cubiq_validate_password_reset ($errors, $user) {
	// passwords don't match
	if ( $errors->get_error_code() ) {

		wp_redirect( home_url('/login/?action=resetpass&failed=nomatch') );
		
		exit;
	}

	// wp-login already checked if the password is valid, so no further check is needed
	if ( !empty( $_POST['pass1'] ) ) {

		reset_password($user, $_POST['pass1']);

		wp_redirect( home_url('/login/?action=resetpass&success=1') );

		exit;
	}

	// redirect to change password form
	wp_redirect( home_url('/login/?action=resetpass') );

	exit;
}
add_action('validate_password_reset', 'cubiq_validate_password_reset', 10, 2);

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'front_extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>

    <h3><?php _e("Extra profile information", karisma_text_domain); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="property_group"><?php _e("Property Group", karisma_text_domain); ?></label></th>
        <td>
            <input type="text" name="property_group" id="property_group" value="<?php echo esc_attr( get_the_author_meta( 'property_group', $user->ID ) ); ?>" class="regular-text form-control" readonly/><br />
            <span class="description"><?php _e("Please enter propery group."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="property_name"><?php _e("Property Name", karisma_text_domain); ?></label></th>
        <td>
            <input type="text" name="property_name" id="property_name" value="<?php echo esc_attr( get_the_author_meta( 'property_name', $user->ID ) ); ?>" class="regular-text form-control" readonly /><br />
            <span class="description"><?php _e("Please enter propery name."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="total_rooms"><?php _e("Rooms"); ?></label></th>
        <td>
            <input type="number" name="total_rooms" id="total_rooms" value="<?php echo esc_attr( get_the_author_meta( 'total_rooms', $user->ID ) ); ?>" class="regular-text form-control" /><br />
            <span class="description"><?php _e("Please enter Rooms."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="book_avg"><?php _e("Book Avg"); ?></label></th>
        <td>
            <input type="number" name="book_avg" id="book_avg" value="<?php echo esc_attr( get_the_author_meta( 'book_avg', $user->ID ) ); ?>" class="regular-text form-control" /><br />
            <span class="description"><?php _e("Please enter Book Avg."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="web_managed_by"><?php _e("Webmaster"); ?></label></th>
        <td>
            <input type="text" name="web_managed_by" id="web_managed_by" value="<?php echo esc_attr( get_the_author_meta( 'web_managed_by', $user->ID ) ); ?>" class="regular-text form-control" /><br />
            <span class="description"><?php _e("Please enter Webmaster."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="pic_name"><?php _e("PIC Name"); ?></label></th>
        <td>
            <input type="text" name="pic_name" id="pic_name" value="<?php echo esc_attr( get_the_author_meta( 'pic_name', $user->ID ) ); ?>" class="regular-text form-control" /><br />
            <span class="description"><?php _e("Please enter PIC name."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="office_phone"><?php _e("Office Phone"); ?></label></th>
        <td>
            <input type="text" name="office_phone" id="office_phone" value="<?php echo esc_attr( get_the_author_meta( 'office_phone', $user->ID ) ); ?>" class="regular-text form-control" /><br />
            <span class="description"><?php _e("Please enter Office Phone."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="whatsapp"><?php _e("whatsapp"); ?></label></th>
        <td>
            <input type="text" name="whatsapp" id="whatsapp" value="<?php echo esc_attr( get_the_author_meta( 'whatsapp', $user->ID ) ); ?>" class="regular-text form-control" /><br />
            <span class="description"><?php _e("Please enter whatsapp."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="package_scheme"><?php _e("Package"); ?></label></th>
        <td>
            <label>
                <input type="radio" name="package_scheme" class="input radio-inline" value="template" <?php checked( get_the_author_meta( 'package_scheme', $user->ID ), "template" ); ?>> Template
            </label>
			<br>
            <label>
                <input type="radio" name="package_scheme" class="input radio-inline" value="booking-engine" <?php checked( get_the_author_meta( 'package_scheme', $user->ID ), "booking-engine" ); ?>> Boking Engine
            </label>
			<br>
            <label>
                <input type="radio" name="package_scheme" class="input radio-inline" value="booking-engine-only" <?php checked( get_the_author_meta( 'package_scheme', $user->ID ), "booking-engine-only" ); ?>> Booking Engine Only
            </label>
        </td>
    </tr>
	 <tr>
    	<th><label for="payment_scheme"><?php _e("Payment Scheme"); ?></label></th>
        <td>
            <label>
                <input type="radio" name="payment_scheme" class="input radio-inline" value="1" <?php checked( get_the_author_meta( 'payment_scheme', $user->ID ), "1" ); ?>> Variable Booking Commission
            </label>
			<br>
            <label>
                <input type="radio" name="payment_scheme" class="input radio-inline" value="2" <?php checked( get_the_author_meta( 'payment_scheme', $user->ID ), "2" ); ?>> Fixed Mothly Fee
            </label>
        </td>
    </tr>
	<input type="hidden" name="faq" id="faq" value="<?php echo esc_attr( get_the_author_meta( 'faq', $user->ID ) ); ?>" class="regular-text" /><br />
    </table>
<?php }
function front_extra_user_profile_fields( $user ){ ?>
    <h3><?php _e("Extra profile information", karisma_text_domain); ?></h3>
  	<input type="hidden" name="faq" id="faq" value="<?php echo esc_attr( get_the_author_meta( 'faq', $user->ID ) ); ?>" class="regular-text" /><br />

  <div class="form-group row">
    <label for="property_group" class="col-sm-2 col-form-label">Property Group</label>
    <div class="col-md-10">
            <input type="text" name="property_group" id="property_group" value="<?php echo esc_attr( get_the_author_meta( 'property_group', $user->ID ) ); ?>" class="regular-text form-control" readonly/><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="property_name" class="col-sm-2 col-form-label">Property Name</label>
    <div class="col-md-10">
            <input type="text" name="property_name" id="property_name" value="<?php echo esc_attr( get_the_author_meta( 'property_name', $user->ID ) ); ?>" class="regular-text form-control" readonly /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="total_rooms" class="col-sm-2 col-form-label">Rooms</label>
    <div class="col-md-10">
            <input type="text" name="total_rooms" id="total_rooms" value="<?php echo esc_attr( get_the_author_meta( 'total_rooms', $user->ID ) ); ?>" class="regular-text form-control" /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="book_avg" class="col-sm-2 col-form-label">Book Average</label>
    <div class="col-md-10">
            <input type="text" name="book_avg" id="book_avg" value="<?php echo esc_attr( get_the_author_meta( 'book_avg', $user->ID ) ); ?>" class="regular-text form-control"  /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="web_managed_by" class="col-sm-2 col-form-label">Webmaster</label>
    <div class="col-md-10">
            <input type="text" name="web_managed_by" id="web_managed_by" value="<?php echo esc_attr( get_the_author_meta( 'web_managed_by', $user->ID ) ); ?>" class="regular-text form-control"  /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="pic_name" class="col-sm-2 col-form-label">PIC Name</label>
    <div class="col-md-10">
            <input type="text" name="pic_name" id="pic_name" value="<?php echo esc_attr( get_the_author_meta( 'pic_name', $user->ID ) ); ?>" class="regular-text form-control"  /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="office_phone" class="col-sm-2 col-form-label">Office Phone</label>
    <div class="col-md-10">
            <input type="text" name="office_phone" id="office_phone" value="<?php echo esc_attr( get_the_author_meta( 'office_phone', $user->ID ) ); ?>" class="regular-text form-control"  /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="whatsapp" class="col-sm-2 col-form-label">Whatsapp</label>
    <div class="col-md-10">
            <input type="text" name="whatsapp" id="whatsapp" value="<?php echo esc_attr( get_the_author_meta( 'whatsapp', $user->ID ) ); ?>" class="regular-text form-control"  /><br />
    </div>
  </div>
  <div class="form-group row">
    <label for="package_scheme" class="col-sm-2 col-form-label">Package scheme</label>
    <div class="col-md-10">
            <label>
                <input type="radio" name="package_scheme" class="input radio-inline" value="template" <?php checked( get_the_author_meta( 'package_scheme', $user->ID ), "template" ); ?>> Template
            </label>
			<br>
            <label>
                <input type="radio" name="package_scheme" class="input radio-inline" value="booking-engine" <?php checked( get_the_author_meta( 'package_scheme', $user->ID ), "booking-engine" ); ?>> Boking Engine
            </label>
			<br>
            <label>
                <input type="radio" name="package_scheme" class="input radio-inline" value="booking-engine-only" <?php checked( get_the_author_meta( 'package_scheme', $user->ID ), "booking-engine-only" ); ?>> Booking Engine Only
            </label>    
		</div>
  </div>
  <div class="form-group row">
    <label for="payment_scheme" class="col-sm-2 col-form-label">Payment scheme</label>
    <div class="col-md-10">
            <label>
                <input type="radio" name="payment_scheme" class="input radio-inline" value="1" <?php checked( get_the_author_meta( 'payment_scheme', $user->ID ), "1" ); ?>> Variable Booking Commission
            </label>
			<br>
            <label>
                <input type="radio" name="payment_scheme" class="input radio-inline" value="2" <?php checked( get_the_author_meta( 'payment_scheme', $user->ID ), "2" ); ?>> Fixed Mothly Fee
            </label>
		</div>
  </div>
<?php }
//action for save extra user profile 
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );


function save_extra_user_profile_fields( $user_id ) {
	
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
	}
	if ( isset( $_POST['property_group'] ) ) {
		update_user_meta( $user_id, 'property_group', $_POST['property_group'] );
	}
	if ( isset( $_POST['property_name'] ) ) {
		update_user_meta( $user_id, 'property_name', $_POST['property_name'] );
	}
	if ( isset( $_POST['total_rooms'] ) ) {
		update_user_meta( $user_id, 'total_rooms', $_POST['total_rooms'] );
	}
	if ( isset( $_POST['book_avg'] ) ) {
		update_user_meta( $user_id, 'book_avg', $_POST['book_avg'] );
	}
	if ( isset( $_POST['web_managed_by'] ) ) {
		update_user_meta( $user_id, 'web_managed_by', $_POST['web_managed_by'] );
	}
	if ( isset( $_POST['pic_name'] ) ) {
		update_user_meta( $user_id, 'pic_name', $_POST['pic_name'] );
	}
	if ( isset( $_POST['office_phone'] ) ) {
		update_user_meta( $user_id, 'office_phone', $_POST['office_phone'] );
	}
	if ( isset( $_POST['whatsapp'] ) ) {
		update_user_meta( $user_id, 'whatsapp', $_POST['whatsapp'] );
	}
	if ( isset( $_POST['faq'] ) ) {
		update_user_meta( $user_id, 'faq', $_POST['faq'] );
	}
	if ( isset( $_POST['payment_scheme'] ) ) {
		update_user_meta( $user_id, 'payment_scheme', $_POST['payment_scheme'] );
	}
	if ( isset( $_POST['package_scheme'] ) ) {
		update_user_meta( $user_id, 'package_scheme', $_POST['package_scheme'] );
	}
	do_action('set_data_property' );

}
add_action('user_register', 'register_extra_user_profile_fields');

function register_extra_user_profile_fields($user_id){
	$user = new WP_User( $user_id );  
	if(isset($_POST['wp-submit'])){
		foreach( $user->roles as $role ) { 
			if ( $role === 'subscriber' ) {
				update_user_meta( $user_id, 'property_group', $_POST['property_group'] );
				update_user_meta( $user_id, 'property_name', $_POST['property_name'] );
				update_user_meta( $user_id, 'total_rooms', $_POST['total_rooms'] );
				update_user_meta( $user_id, 'book_avg', $_POST['book_avg'] );
				update_user_meta( $user_id, 'web_managed_by', $_POST['web_managed_by'] );
				update_user_meta( $user_id, 'pic_name', $_POST['pic_name'] );
				update_user_meta( $user_id, 'office_phone', $_POST['office_phone'] );
				update_user_meta( $user_id, 'whatsapp', $_POST['whatsapp'] );
				update_user_meta( $user_id, 'faq', $_POST['faq'] );
				update_user_meta( $user_id, 'payment_scheme', $_POST['payment_scheme'] );
				update_user_meta( $user_id, 'package_scheme', $_POST['package_scheme'] );
				update_user_meta( $user_id, 'id_invoice', 'IDS-INV-'.date("m/Y").'-'.$user_id );
			}
		}
	}

}


add_action('register_form', 'extra_user_register_fields');

function extra_user_register_fields ( $user){ ?>
    <p>
        <label for="property_group">Property Group</label>
        <input type="text" name="property_group" id="property_group" class="input" value="">
    </p>

    <p>
        <label for="property_name">Property Name</label>
        <?php do_action('idh_data_hotel'); ?>
    </p>
    <p>
        <label for="total_rooms">Rooms</label>
        <input type="text" name="total_rooms" id="total_rooms" class="input" value="">
    </p>
    <p>
        <label for="book_avg">Average Booking/label>
        <input type="text" name="book_avg" id="book_avg" class="input" value="">
    </p>
    <p>
        <label for="web_managed_by">Website managed by</label>
        <input type="text" name="web_managed_by" id="web_managed_by" class="input" value="">
    </p>
    <p>
        <label for="pic_name">PIC name</label>
        <input type="text" name="pic_name" id="pic_name" class="input" value="">
    </p>
    <p>
        <label for="package">Package</label>
        <input type="text" name="package_scheme" id="package_scheme" class="input" value="">
    </p>
    <p>
        <label for="payment_scheme">Payment Scheme</label>
        <label>
            <input type="radio" name="payment_scheme" class="input" value="1"> Variable Booking Commission
        </label>
        <label>
            <input type="radio" name="payment_scheme" class="input" value="2"> Fixed Mothly Fee
        </label>
    </p>
<?php }



function registration_email_alert( $user_id ) {
	if(isset($user_id) && !empty($user_id)){
		$user    = get_userdata( $user_id );
		$email   = $user->user_email;
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$message = '';
		
		wp_mail($email, 'subject', $message, $headers);
	}
}
add_action('user_register', 'registration_email_alert');

require_once child_dir.'/inc/_custom_post.php';

$product = array(
    'slug' => 'product',
    'name' => 'Product',
	'singular_name' => 'Product',
	'menu_position' => 2,
    'public' => true,
    'has_archive' => true,
);
$portfolio = array(
    'slug' => 'portfolio',
    'name' => 'Portfolio',
	'singular_name' => 'portfolio',
	'menu_position' => 2,
    'public' => true,
	'has_archive' => true,
);

 
$product = new Custom_Posts ($product);
$portfolio = new Custom_Posts ($portfolio);
