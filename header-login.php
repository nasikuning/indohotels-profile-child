
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

	<?php wp_head(); ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body <?php body_class(); ?>>
  <header>
  <div class="section-top">
    <?php
    $action = !empty( $_GET['action'] ) && ($_GET['action'] == 'register' || $_GET['action'] == 'forgot' || $_GET['action'] == 'resetpass') ? $_GET['action'] : 'login';
    $success = !empty( $_GET['success'] );
    $failed = !empty( $_GET['failed'] ) ? $_GET['failed'] : false;

    ?>
    <?php if ( !$success && $action != 'resetpass' ): ?>
       <div class="login-section">
           <div id="login-tabs" class="container text-right ">
                <span class="<?php if ($action == 'login') echo 'active-tab'; ?>"><a href="#tab-login">Login</a></span>
                <span>|</span>
                <span class="<?php if ($action == 'register') echo 'active-tab'; ?>"><a href="#tab-register">Register</a></span>
                <span>|</span>
                <span class="<?php if ($action == 'forgot') echo 'active-tab'; ?>"><a href="#tab-forgot">Forgot?</a></span>
            </div>
        </div>
    <?php endif; ?>
    <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button> -->
          
          <button type="button" class="navbar-toggle" onclick="openNav()">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="top-logo pull-left">
          <a href="<?php echo get_home_url(); ?>" class="logo-white"><img src="<?php echo ot_get_option('krs_logo'); ?>" alt="" class="img-responsive"></a>
          <a href="<?php echo get_home_url(); ?>" class="logo-black"><img src="<?php echo ot_get_option('krs_logo2'); ?>" alt="" class="img-responsive"></a>
        </div>
        <div class="navbar-collapse collapse">
          <?php karisma_nav(); ?>
        </div>
        <!-- mobile menu -->
        <div class="mobmenu">
          <div id="mobinav" class="sidenav">
            <div class="mobinav-inner login">
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
              <?php karisma_nav_mobile(); ?>
            </div>
          </div><!-- end .mobinav -->
          <span class="icon-list-add mobico" onclick="openNav()"></span>
        </div><!-- end .mobmenu -->
      </div>
    </div>
  </div><!-- end .section-top -->

</header>