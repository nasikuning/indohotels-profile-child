<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

	<?php wp_head(); ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body <?php body_class(); ?>>
<div class="header">
<?php 
    global $current_user;
    wp_get_current_user();
?>
    <div class="logo">
          <a href="<?php echo get_site_url(); ?>" class="logo-black"><img src="<?php echo ot_get_option('krs_logo2'); ?>" alt="" class="img-responsive"></a>
    </div>
        <nav class="navbar navbar-inverse">
          <div class="container-fluid">
                  <div class="navbar-header">
          <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button> -->
          <button type="button" class="navbar-toggle" onclick="openNav()">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
                <div class="collapse navbar-collapse">
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle" onclick="openNav()">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="<?php echo get_site_url(); ?>/member/">Dashboard</a>
                    </div>
                    <?php krs_member_nav(); ?>
                    <?php krs_member_user_nav(); ?>
                    <!-- <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php site_url(); ?>/profile/"><span class="glyphicon glyphicon-user"></span> <?php echo $current_user->display_name ?></a></li>
                        <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="glyphicon glyphicon-log-in"></span> logout</a></li>
                    </ul> -->
                </div>
                <!-- mobile menu -->
                <div class="mobmenu">
                <div id="mobinav" class="sidenav">
                    <div class="mobinav-inner login">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <?php krs_member_nav_mobile(); ?>
                    <?php krs_member_user_nav_mobile(); ?>
                    </div>
                </div><!-- end .mobinav -->
                <span class="icon-list-add mobico" onclick="openNav()"></span>
                </div><!-- end .mobmenu -->
          </div>
        </nav>
</div>