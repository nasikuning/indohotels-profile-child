<?php

class Custom_Posts {
 
    function __construct($init) {
 
        $this->settings = $init;
 
        add_action( 'init', array(&$this, 'add_custom_post_type') );
 
    }
 
     function add_custom_post_type() {
 
        register_post_type( $this->settings['slug'],
 
        array(
            'labels' => array(
                'name' => __( $this->settings['name'] ),
                'singular_name' => __( $this->settings['singular_name'] )
            ),
 
        'public' => $this->settings['public'],
 
        'has_archive' => $this->settings['has_archive'],
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), 
        )
 
    );
 
    }
}