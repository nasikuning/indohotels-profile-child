<?php           
        get_template_part('partials/stripe', 'slider');

            // are there any rows within within our flexible content?
            if( have_rows('section') ): 

            // loop through all the rows of flexible content
            while ( have_rows('section') ) : the_row();

                    if( get_row_layout() == 'portfolio' ) {
                        get_template_part('partials/stripe', 'portfolio');
                    }

                    if( get_row_layout() == 'text_slider' ) {
                        get_template_part('partials/stripe', 'text');
                    }

                    if( get_row_layout() == 'gallery_section' ) {
                        get_template_part('partials/stripe', 'gallery');
                    }

                    if( get_row_layout() == 'contact_section' ) {
                        get_template_part('partials/stripe', 'contact');
                    }

                    if( get_row_layout() == 'address_section' ) {
                        get_template_part('partials/stripe', 'address');
                    }

                    if( get_row_layout() == 'about_section' ) {
                        get_template_part('partials/stripe', 'about');
                    }

                    if( get_row_layout() == 'location' ) {
                        get_template_part('partials/stripe', 'maps');
                    }


            endwhile; // close the loop of flexible content
            
            endif; // close flexible content conditional
?>