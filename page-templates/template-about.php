<?php /* Template Name: About Template */ get_header('home'); ?>

<div id="wrapper" class="page">
      <?php

        if (have_posts()) : while (have_posts()) : the_post();

          get_template_part('partials/stripe', 'slider'); ?>

          <div class="content-single">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="whoweare">
                <div class="container">
                  <h2><?php the_title(); ?></h2>
                  <div class="content-desc">
                      <?php the_content(); ?>
                  </div> <!-- content-desc -->
                </div>
              </div>
            </div>
          </div>
          
        <?php endwhile; endif; // close the WordPress loop
        ?>

  </div><!-- end .content -->
  <?php get_footer(); ?>