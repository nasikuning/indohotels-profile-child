<?php /* Template Name: Dashboard Template */ get_header('member');
?>
        <div id="page-wrapper">
            <div class="container">
                    <div class="bg-title">
                        <h2 class="page-title" style="font-weight: bold;">
                            <i class="fa fa-home"></i> <?php the_title(); ?>
                        </h2>
                    </div>
                <?php echo do_shortcode('[idh_dashboard]'); ?>
            </div>
        </div>

<?php get_footer('member'); ?>
