<?php /* Template Name: Home Template */ get_header('home'); ?>

  <div id="wrapper" class="homepage">

    <div class="section main-slider">
      <div id="slider-main" class="owl-carousel">
        <div class="owl-slide" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/1.jpg')">
          <div class="container hidden-xs">
            <div class="text-slider">
              <h2>Indohotels Group</h2>
              <p>As the digital world growing fast, the comprehensive solution to boost hotels revenue need exist with the affordable, user-friendly, and reliable. We are the one who come to be hotels partner in digital solution.</p>
              <div class="read-more">
                <a href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="owl-slide" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/2.jpg')">
          <div class="container hidden-xs">
            <div class="text-slider">
              <h2>Indohotels Group</h2>
              <p>As the digital world growing fast, the comprehensive solution to boost hotels revenue need exist with the affordable, user-friendly, and reliable. We are the one who come to be hotels partner in digital solution.</p>
              <div class="read-more">
                <a href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="owl-slide" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/3.jpg')">
          <div class="container hidden-xs">
            <div class="text-slider">
              <h2>Indohotels Group</h2>
              <p>As the digital world growing fast, the comprehensive solution to boost hotels revenue need exist with the affordable, user-friendly, and reliable. We are the one who come to be hotels partner in digital solution.</p>
              <div class="read-more">
                <a href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="owl-slide" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/4.jpg')">
          <div class="container hidden-xs">
            <div class="text-slider">
              <h2>Indohotels Group</h2>
              <p>As the digital world growing fast, the comprehensive solution to boost hotels revenue need exist with the affordable, user-friendly, and reliable. We are the one who come to be hotels partner in digital solution.</p>
              <div class="read-more">
                <a href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="owl-slide" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/5.jpg')">
          <div class="container hidden-xs">
            <div class="text-slider">
              <h2>Indohotels Group</h2>
              <p>As the digital world growing fast, the comprehensive solution to boost hotels revenue need exist with the affordable, user-friendly, and reliable. We are the one who come to be hotels partner in digital solution.</p>
              <div class="read-more">
                <a href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
      </div><!-- end .slider-main -->
    </div><!-- end .main-slider -->

    <div class="container home">
      <div class="section main-service">
        <div class="row-list">
          <div class="list">
            <div class="outer-service text-center">
              <div class="img-service">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-indohotels.svg" alt="">
              </div>
              <div class="title-service"></div>
              <div class="content-service">
                <p>
                  Online Booking Platform for Property in Indonesia
                </p>
                <a href="#">+ READ MORE</a>
              </div>
            </div>
          </div>

          <div class="list">
            <div class="outer-service text-center">
              <div class="img-service">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-mytripbagus.svg" alt="">
              </div>
              <div class="title-service"></div>
              <div class="content-service">
                <p>
                  OTA Comparison for hotels & flights
                </p>
                <a href="#">+ READ MORE</a>
              </div>
            </div>
          </div>

          <div class="list">
            <div class="outer-service text-center">
              <div class="img-service">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-hotelsdirect.svg" alt="">
              </div>
              <div class="title-service"></div>
              <div class="content-service">
                <p>
                  Booking engine provider to help hotel to increase online revenue
                </p>
                <a href="#">+ READ MORE</a>
              </div>
            </div>
          </div>

          <!-- <div class="list">
            <div class="outer-service text-center">
              <div class="img-service">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-starfusion.svg" alt="">
              </div>
              <div class="title-service"></div>
              <div class="content-service">
                <p>
                  Channel manager to help hotel managing many online reservation distributions
                </p>
                <a href="#">+ READ MORE</a>
              </div>
            </div>
          </div> -->

          <div class="list">
            <div class="outer-service text-center">
              <div class="img-service">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-booklogic.svg" alt="">
              </div>
              <div class="title-service"></div>
              <div class="content-service">
                <p>
                  Save cost on manpower. Automatic & efficientn. No more human errors.
                </p>
                <a href="#">+ READ MORE</a>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>
        </div>
      </div><!-- end -->
      <div class="section top-subscription">
        <div class="row">
          <div class="col-md-6">
            <div class="left-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/asset/img/portfolio/hotel/eastparc.jpg')"></div>
          </div>
          <div class="col-md-6">
            <form action="page-package.html" method="">
              <div class="right-form">
                <label>Grow Your Revenue Trough Direct Online Booking</label>
                <input class="form-control" type="text" name="email" id="email" value="" placeholder="please insert your email here">
                <div class="button">
                  <button class="btn" type="submit" name="button" id="subscribe" name="subscribe">REGISTER NOW</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="section smart-system">
        <h2>3 Reason to Choose Us</h2>
        <div class="row">
          <div class="col-sm-6">
            <div class="left-image">
              <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/new/landing-page.png" alt="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="right-content">
              <ul class="list-check">
                <li>Local base, our headquarter located in Yogyakarta is ready to any solutions needed.</li>
                <li>Experienced team, we are supported by professional in-house both developers & marketing team with major experience in hospitality & digital marketing</li>
                <li>ROI Minded, you can count your ROI faster using our service</li>
              </ul>
            </div>
          </div>
        </div>
      </div><!-- end -->
    </div><!-- end .container -->
    <div class="bottom-section-white">
      <div class="container">

        <!-- <div class="section our-team">
          <div class="grid-our-team">
            <div class="left-title">
              <img src="asset/img/team/all-team.jpg" alt="">
              <div class="overlay">
                <a href="page-about.html">
                  <span>OUR</span>
                  <span>TEAM</span>
                </a>
              </div>
            </div>
            <div class="right-team">
              <div class="staff">
                <div class="outer-staff">
                  <img src="asset/img/team/team-male.jpg" alt="">
                  <div class="overlay">
                    <div class="staff-desc">
                      <div class="name">Ginanjar Budhiraharja</div>
                      <div class="position">Co-Founder</div>
                      <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="staff">
                <div class="outer-staff">
                  <img src="asset/img/team/team-male.jpg" alt="">
                  <div class="overlay">
                    <div class="staff-desc">
                      <div class="name">Habib Yasien</div>
                      <div class="position">Senior Sales Representative</div>
                      <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="staff">
                <div class="outer-staff">
                  <img src="asset/img/team/team-male.jpg" alt="">
                  <div class="overlay">
                    <div class="staff-desc">
                      <div class="name">Sidiq Aldi Ginanjar</div>
                      <div class="position">System Architect</div>
                      <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="staff">
                <div class="outer-staff">
                  <img src="asset/img/team/team-male.jpg" alt="">
                  <div class="overlay">
                    <div class="staff-desc">
                      <div class="name">Sudhir Abdul Rahman</div>
                      <div class="position">Advisor</div>
                      <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>-->

        <div class="section content-portfolio">
          <h2>Portfolio</h2>
          <div class="outer-random-portfolio">
            <div class="portfolio-list">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide-hotel-eastparc.jpg" alt="">
              <div class="overlay">
                <div class="desc">
                  <h2><a href="page-portfolio-hotel.html">Hotel Website</a></h2>
                  <!-- <p><a href="#"><i class="fa fa-globe"></i> Visit site</a></p>
                  <div class="detail-button">
                    <a href="#">DETAIL</a>
                  </div> -->
                </div>
              </div>
            </div>
            <div class="portfolio-list">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/newsaphir.jpg" alt="">
              <div class="overlay">
                <div class="desc">
                  <h2><a href="page-portfolio-ota.html">Booking Engine</a></h2>
                  <!-- <p><a href="#"><i class="fa fa-globe"></i> Visit site</a></p>
                  <div class="detail-button">
                    <a href="#">DETAIL</a>
                  </div> -->
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div><!-- end -->
        <div class="section bottom-subscription">
          <form action="page-package.html" method="">
            <div class="right-form">
              <label>Grow Your Revenue Trough Direct Online Booking</label>
              <input class="form-control" type="text" name="email" is="email2" value="" placeholder="please insert your email here">
              <div class="button">
                <button class="btn" type="submit" name="button" id="subscribe_buttom" name="subscribe_buttom">REGISTER NOW</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div><!-- end .content -->

<?php get_footer(); ?>
