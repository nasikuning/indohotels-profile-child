<?php do_action('set_pdf'); ?>

<?php /* Template Name: Invoice Template */ get_header('member'); ?>
<div id="page-wrapper">
    <div class="container">
            <div class="bg-title">
                    <h2 class="page-title" style="font-weight: bold;">
                        <i class="fa fa-home"></i> <?php the_title(); ?>
                    </h2>
			</div>
        <div class="panel panel-info">
            <div class="panel-body">
                <?php echo do_shortcode('[idh_invoice]'); ?>
            </div>
        </div>

    </div>
</div>

<?php get_footer('member'); ?>
