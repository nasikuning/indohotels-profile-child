<?php /* Template Name: Login Template */ get_header('login'); ?>
<?php
$action = !empty( $_GET['action'] ) && ($_GET['action'] == 'register' || $_GET['action'] == 'forgot' || $_GET['action'] == 'resetpass') ? $_GET['action'] : 'login';
$success = !empty( $_GET['success'] );
$failed = !empty( $_GET['failed'] ) ? $_GET['failed'] : false;

?>
<div id="wrapper" class="page">
    <?php

        if (have_posts()) : while (have_posts()) : the_post();

          get_template_part('partials/stripe', 'slider');

        endwhile; endif; // close the WordPress loop
    ?>
    <div class="login-package">

        <div class="container">

            <div class="outer-package-page">
                        <!-- <div class="logo">
          <a href="<?php echo get_home_url(); ?>" class="logo-black"><img src="<?php echo ot_get_option('krs_logo2'); ?>" alt="" class="img-responsive"></a>
                        </div> -->
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php if ( $action == 'register' && $success ): ?>

                    <div class="register-success">
                        <div class="entry-header">
                            <h1>Success!</h1>
                        </div>

                        <div class="message-box message-success">
                            <span class="icon-thumbs-up"></span>
                            Check your email for the password and then return to <a href="<?php echo get_site_url(); ?>/login/?action=login">log in</a>.
                        </div>
                    </div>

                    <?php elseif ( $action == 'forgot' && $success ): ?>

                    <div class="forgot-success">
                        <div class="entry-header">
                            <h1>Password recovery</h1>
                        </div>

                        <div class="message-box message-info">
                            <span class="icon-bell"></span>
                            Check your email for the instructions to get a new password.
                        </div>
                    </div>

                    <?php elseif ( $action == 'resetpass' && $success ): ?>

                    <div class="resetpass-success">
                        <div class="entry-header">
                            <h1>Password reset</h1>
                        </div>

                        <div class="message-box message-success">
                            <span class="icon-thumbs-up"></span>
                            Your password has been updated.
                            <a href="<?php echo get_site_url(); ?>/login/">Proceed to login</a>.
                        </div>
                    </div>

                    <?php else: ?>

                    <div id="tab-login" class="tab-content1" style="<?php if ( $action != 'login' ) echo 'display:none' ?>">
                      <div class="outer-tab-login">
                        <h3 class="text-center">Login In to Indohotels Group</h3>
                        <?php if ( $action == 'login' && $failed ): ?>
                        <div class="message-box message-error alert alert-danger">
                            <span class="icon-attention"></span>
                            <?php if ( $failed ): ?> Invalid username or password. Please try again.
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>

                        <?php wp_login_form(); ?>

                        <div class="entry-content text-center">
                            <p>Don't have an account?
                                <a href="<?php site_url(); ?>/login/?action=register">Sign up now</a>!</p>
                        </div>
                      </div>
                    </div>

                    <div id="tab-register" class="tab-content1" style="<?php if ( $action != 'register' ) echo 'display:none' ?>">
                      <div class="outer-tab-register">
                        <?php if ( $action == 'register' && $failed ): ?>
                        <div class="message-box message-error">
                            <span class="icon-attention"></span>
                            <?php if ( $failed == 'invalid_character' ): ?> Username can only contain alphanumerical characters, "_" and "-". Please choose another username.
                            <?php elseif ( $failed == 'username_exists' ): ?> Username already in use.
                            <?php elseif ( $failed == 'email_exists' ): ?> E-mail already in use. Maybe you are already registered?
                            <?php elseif ( $failed == 'empty' ): ?> All fields are required.
                            <?php else: ?> An error occurred while registering the new user. Please try again.
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>

                        <h3>Chose Package</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                        <div class="outer-list-package">
                        <div id="template" class="list-package">
                            <div class="border">
                            <i class="fa fa-check-circle product_clickable circle-selected" data-period="template"  data-price="2880000" aria-hidden="true" style="display: none;"></i>
                            <div class="title">
                                <h4>CHOOSE OUR TEMPLATE</h4>
                            </div>
                            <div class="price">
                                <h4>IDR 2.880.000</h4>
                            </div>
                            <div class="desc-list">
                                <ul>
                                <li>Free domain name, hosting, email and optimization</li>
                                <li>Free promotion materials and tent card</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                </ul>
                            </div>
                            <div class="select-button">
                                <a href="#">SELECT</a>
                            </div>
                            <div class="method">
                                CHOOSE ONE PAYMENT METHOD
                            </div>
                            </div>
                        </div><!-- /.list-package -->
                        <div id="booking-engine" class="list-package">
                            <div class="border">
                            <i class="fa fa-check-circle product_clickable circle-selected" data-period="booking-engine"  data-price="960000" aria-hidden="true" style="display: none;"></i>
                            <div class="title">
                                <h4>INSTALL BOOKONG ENGINE & IMPROVEMENT</h4>
                            </div>
                            <div class="price">
                                <h4>IDR 960.000</h4>
                            </div>
                            <div class="desc-list">
                                <ul>
                                <li>Free domain name, hosting, email and optimization</li>
                                <li>Free promotion materials and tent card</li>
                                </ul>
                            </div>
                            <div class="select-button">
                                <a href="#">SELECT</a>
                            </div>
                            <div class="method">
                                CHOOSE ONE PAYMENT METHOD
                            </div>
                            </div>
                        </div><!-- /.list-package -->
                        <div id="booking-engine-only" class="list-package">
                            <div class="border">
                            <i class="fa fa-check-circle product_clickable circle-selected" data-period="booking-engine-only"  data-price="0" aria-hidden="true" style="display: none;"></i>
                            <div class="title">
                                <h4>INSTALL BOOKING ENGINE ONLY</h4>
                            </div>
                            <div class="price">
                                <h4>FREE</h4>
                            </div>
                            <div class="desc-list">
                                <ul>
                                <li>Free domain name, hosting, email and optimization</li>
                                <li>Free promotion materials and tent card</li>
                                </ul>
                            </div>
                            <div class="select-button">
                                <a href="#">SELECT</a>
                            </div>
                            <div class="method">
                                FIXED MOTHLY FEE
                            </div>
                            </div>
                        </div><!-- /.list-package -->
                        <div class="clearfix"></div>
                        </div><!-- /.outer-list-page -->
                        <h3>CHOOSE PAYMENT METHOD</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                        <div id="tabspackage" class="outer-tab-link">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="payment active">
                                <a href="#variable" class="payment_selected" aria-controls="variable" role="tab" data-toggle="tab" data-payment="1">
                                    <span class="title">Variable Booking Commission</span>
                                </a>
                                </li>
                                <li role="presentation" class="payment">
                                <a href="#fixed" class="payment_selected" aria-controls="fixed" role="tab" data-toggle="tab" data-payment="2">
                                    <span class="title">Fixed Mothly Fee</span>
                                </a>
                                </li>
                            </ul>
                        </div> <!-- /#tabspackage -->
                        <div class="outer-content-tab tab-content">
                            <div role="tabpanel" class="tab-pane active" id="variable">
                              <div class="table-responsive">
                                <table>
                                <tbody>
                                    <tr class="bg-grey">
                                    <td rowspan="2" class="bg-orange">BOOKINGS PER MONTH</td>
                                    <td>FIRST 50 BOOKINGS</td>
                                    <td>NEXT 50 BOOKINGS</td>
                                    <td>THEREAFTER</td>
                                    </tr>
                                    <tr class="bg-light-grey">
                                    <td>1-50</td>
                                    <td>50-100</td>
                                    <td>> 101</td>
                                    </tr>
                                    <tr class="bg-light-grey">
                                    <td class="bg-orange">PRICE</td>
                                    <td>8%</td>
                                    <td>6%</td>
                                    <td>4%</td>
                                    </tr>
                                </tbody>
                                </table>
                              </div>
                                <div class="payment-gateway-fee">
                                + 4% Payment Gateway Fee Per Transaction
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="fixed">
                              <div class="table-responsive">
                                <table>
                                <tbody>
                                    <tr>
                                    <td colspan="7" class="text-center">MONTHLY FEES (AFTER 50% DISCOUNT)</td>
                                    </tr>
                                    <tr class="bg-light-grey">
                                    <td class="bg-grey">HOTEL ROOMS</td>
                                    <td>< 100</td>
                                    <td>101 - 150</td>
                                    <td>151 - 200</td>
                                    <td>201 - 250</td>
                                    <td>251 - 300</td>
                                    <td>> 301</td>
                                    </tr>
                                    <tr class="bg-light-grey">
                                    <td class="bg-grey">PRICE</td>
                                    <td>IDR 990,000</td>
                                    <td>IDR 1,490,000</td>
                                    <td>IDR 1,990,000</td>
                                    <td>IDR 2,490,000</td>
                                    <td>IDR 2,990,000</td>
                                    <td>IDR 3,990,000</td>
                                    </tr>
                                </tbody>
                                </table>
                              </div>
                                <table class="payment-gateway-fee">
                                <tr>
                                    <td>+ 4% Payment Gateway Fee Per Transaction</td>
                                </tr>
                                </table>
                            </div>
                        </div>
                        <div class="other-form-information">
                            <h3>YOUR INFORMATION</h3>
                            <?php do_action('user_register'); ?>
                            <form name="registerform" id="registerform" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>" method="post">
                            <input type="hidden" name="package_scheme" id="package_scheme" class="form-control"  value="" >
                            <input type="hidden" name="payment_scheme" id="payment_scheme" class="form-control"  value="" >

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Property Group</label>
                                            <input type="text" name="property_group" id="property_group" class="form-control"  value="" placeholder="Enter Property Group">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Property Name</label>
                                            <?php do_action('idh_data_hotel'); ?>
                                            <!-- <input required type="text" name="property_name" id="property_name" class="form-control"  value="" placeholder="Enter Property Name"> -->
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                    <div class="col-sm-6">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Number Of Rooms</label>
                                            <input required type="number" name="total_rooms" id="total_rooms" min="0" max="1000000" class="form-control"  value="" placeholder="Enter number of rooms">
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                    <div class="col-sm-6">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Average booking per month</label>
                                            <input required type="number"name="book_avg" id="book_avg" min="0" max="1000000" class="form-control"  value="" placeholder="Avg booking per month">
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                    <div class="col-sm-6">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Website managed by</label>
                                            <input type="text" name="web_managed_by" id="web_managed_by" class="form-control"  value="" placeholder="Who is manage your website?">
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>PIC Name</label>
                                            <input required type="text" name="pic_name" id="pic_name" class="form-control"  value="" placeholder="Enter Main Contact Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>PIC Username</label>
                                            <input required type="text" name="user_login" id="user_login" class="form-control" value="" placeholder="Enter username for login">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input required type="email" name="user_email" id="user_email" class="form-control"  value="" placeholder="Enter Property Email">
                                            <input style="display:none" type="text" name="confirm_email" id="confirm_email" class="form-control"  value="" placeholder="Please leave this field empty">
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                    <div class="col-sm-3">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Office Phone</label>
                                            <input type="text" name="office_phone" id="office_phone" class="form-control"  value="" placeholder="Enter Office Phone">
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                    <div class="col-sm-3">
                                        <!-- .col- -->
                                        <div class="form-group">
                                            <label>Whatsapp Number</label>
                                            <input type="text" name="whatsapp" id="whatsapp" class="form-control"  value="" placeholder="Enter Whatsapp Phone">
                                        </div>
                                    </div>
                                    <!-- /.col- -->
                                </div>
                                <div class="checkbox text-center">
                                    <label>
                                        <input type="hidden" name="redirect_to" value="<?php echo get_site_url(); ?>/login/?action=register&amp;success=1" />
                                        <input type="checkbox" name="faq" id="faq" required> I have read and agree to the
                                        <a href="#">Terms & Conditions</a>
                                    </label>
                                </div>
                                <div class="button-area text-center">
                                    <button type="submit" name="wp-submit" id="wp-submit" class="btn btn-default">REGISTER</button>
                                    <div class="already">
                                        Already have account?
                                        <a href="#">Please login here</a>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- ./other-form-information -->
                      </div>
                    </div>
                    <div id="tab-forgot" class="tab-content1" style="<?php if ( $action != 'forgot' ) echo 'display:none' ?>">
                      <div class="outer-tab-forgot">
                        <?php if ( $action == 'forgot' && $failed ): ?>
                        <div class="message-box message-error">
                            <span class="icon-attention"></span>
                            <?php if ( $failed == 'wrongkey' ): ?> The reset key is wrong or expired. Please check that you used the right reset link or request
                            a new one.
                            <?php else: ?> Sorry, we couldn't find any user with that username or email.
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                        <div class="entry-header">
                            <h3 class="entry-title">Password recovery</h3>
                        </div>

                        <div class="entry-content">
                            <div class="alert alert-warning">Please enter your username or email address. You will receive a link to create a new password.</div>
                        </div>

                        <form name="lostpasswordform" id="lostpasswordform" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>"
                            method="post">
                            <p>
                                <label for="user_login">Username or E-mail:</label>
                                <input type="text" name="user_login form-control" id="user_login" class="input" value="">
                            </p>

                            <input type="hidden" name="redirect_to" value="<?php site_url(); ?>/login/?action=forgot&amp;success=1">
                            <p class="submit">
                                <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Get New Password"
                                />
                            </p>
                        </form>
                      </div>
                    </div>
                    <?php if ( $action == 'resetpass' ): ?>
                    <div id="tab-resetpass" class="tab-content">

                        <?php if ( $failed ): ?>
                        <div class="message-box message-error">
                            <span class="icon-attention"></span>
                            The passwords don't match. Please try again.
                        </div>

                        <?php endif; ?>

                        <div class="entry-header">
                            <h1 class="entry-title">Reset password</h1>
                        </div>

                        <div class="entry-content">
                            <p>Create a new password for your account.</p>
                        </div>

                        <form name="resetpasswordform" id="resetpasswordform" action="<?php echo site_url('wp-login.php?action=resetpass', 'login_post') ?>"
                            method="post">
                            <p class="form-password">
                                <label for="pass1">New Password</label>
                                <input class="text-input form-control" name="pass1" type="password" id="pass1">
                            </p>

                            <p class="form-password">
                                <label for="pass2">Confirm Password</label>
                                <input class="text-input form-control" name="pass2" type="password" id="pass2">
                            </p>

                            <input type="hidden" name="redirect_to" value="<?php site_url(); ?>/login/?action=resetpass&amp;success=1">
                            <?php
                                $rp_key = '';
                                $rp_cookie = 'wp-resetpass-' . COOKIEHASH;
                                if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
                                    list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
                                }
                            ?>
                                <!-- <input type="hidden" name="rp_key" value="<?php echo esc_attr( $rp_key ); ?>"> -->
                                <input type="hidden" name="rp_key" value="<?php echo esc_attr( $_GET['key'] ); ?>">

                                <p class="submit">
                                    <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Get New Password"
                                    />
                                </p>
                        </form>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>

                <?php endwhile; ?>
            </div>
            <!-- /.outer-package-page -->

        </div>
        <!-- .container -->
    </div>
</div>
    <!-- #wrapper -->

    <script>
        // tabs
        jQuery('#login-tabs a').click(function (e) {
            e.preventDefault();

            $this = jQuery(this);

            // add class to tab
            jQuery('#login-tabs span').removeClass('active-tab');
            $this.parent().addClass('active-tab');

            // show the right tab
            jQuery('.outer-package-page .tab-content1').hide();
            jQuery('.outer-package-page ' + $this.attr('href')).show();
            return false;
        });
    </script>


    <?php get_footer(); ?>
