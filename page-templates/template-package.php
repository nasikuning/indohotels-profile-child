<?php /* Template Name: Package Template */ get_header('package'); 
$action = !empty( $_GET['action'] ) && ($_GET['action'] == 'register' || $_GET['action'] == 'forgot' || $_GET['action'] == 'resetpass') ? $_GET['action'] : 'login';
$success = !empty( $_GET['success'] );
$failed = !empty( $_GET['failed'] ) ? $_GET['failed'] : false;

?>
<body <?php body_class(); ?>>
  <div id="wrapper" class="page">

    <div class="container">
      <div class="outer-package-page">
        <div class="logo">
          <a href="index.html" class="logo-black"><img src="http://hotel.indohotels.id/companyprofile/asset/img/logo-pt-indohotels.svg" alt="" class="img-responsive"></a>
        </div>
        <h3>CHOOSE PACKAGE</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        <div class="outer-list-package">
          <div id="template" class="list-package">
            <div class="border">
            <i class="fa fa-check-circle product_clickable" data-period="template" aria-hidden="true"></i>
              <div class="title">
                <h4>CHOOSE OUR TEMPLATE</h4>
              </div>
              <div class="price">
                <h4>IDR 2.880.000</h4>
              </div>
              <div class="desc-list">
                <ul>
                  <li>Free domain name, hosting, email and optimization</li>
                  <li>Free promotion materials and tent card</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                </ul>
              </div>
              <div class="select-button">
                <a href="#">SELECT</a>
              </div>
              <div class="method">
                CHOOSE ONE PAYMENT METHOD
              </div>
            </div>
          </div><!-- /.list-package -->

          <div id="booking-engine" class="list-package">
            <div class="border">
            <i class="fa fa-check-circle product_clickable" data-period="booking-engine" aria-hidden="true"></i>
              <div class="title">
                <h4>INSTALL BOOKONG ENGINE & IMPROVEMENT</h4>
              </div>
              <div class="price">
                <h4>IDR 960.000</h4>
              </div>
              <div class="desc-list">
                <ul>
                  <li>Free domain name, hosting, email and optimization</li>
                  <li>Free promotion materials and tent card</li>
                </ul>
              </div>
              <div class="select-button">
                <a href="#">SELECT</a>
              </div>
              <div class="method">
                CHOOSE ONE PAYMENT METHOD
              </div>
            </div>
          </div><!-- /.list-package -->

          <div id="booking-engine-only" class="list-package">
            <div class="border">
            <i class="fa fa-check-circle product_clickable" data-period="booking-engine-only" aria-hidden="true"></i>
              <div class="title">
                <h4>INSTALL BOOKING ENGINE ONLY</h4>
              </div>
              <div class="price">
                <h4>FREE</h4>
              </div>
              <div class="desc-list">
                <ul>
                  <li>Free domain name, hosting, email and optimization</li>
                  <li>Free promotion materials and tent card</li>
                </ul>
              </div>
              <div class="select-button">
                <a href="#">SELECT</a>
              </div>
              <div class="method">
                FIXED MOTHLY FEE
              </div>
            </div>
          </div><!-- /.list-package -->

          <div class="clearfix"></div>
        </div><!-- /.outer-list-page -->
        <h3>CHOOSE PAYMENT METHOD</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        <div id="tabspackage" class="outer-tab-link">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#variable" aria-controls="variable" role="tab" data-toggle="tab">
                <span class="title">Variable Booking Commission</span>
              </a>
            </li>
            <li role="presentation">
              <a href="#fixed" aria-controls="fixed" role="tab" data-toggle="tab">
                <span class="title">Fixed Mothly Fee</span>
              </a>
            </li>
          </ul>
        </div> <!-- /#tabspackage -->
        <div class="outer-content-tab tab-content">
          <div role="tabpanel" class="tab-pane active" id="variable">
            <table>
              <tbody>
                <tr class="bg-grey">
                  <td rowspan="2" class="bg-orange">BOOKINGS PER MONTH</td>
                  <td>FIRST 50 BOOKINGS</td>
                  <td>NEXT 50 BOOKINGS</td>
                  <td>THEREAFTER</td>
                </tr>
                <tr class="bg-light-grey">
                  <td>1-50</td>
                  <td>50-100</td>
                  <td>> 101</td>
                </tr>
                <tr class="bg-light-grey">
                  <td class="bg-orange">PRICE</td>
                  <td>8%</td>
                  <td>6%</td>
                  <td>4%</td>
                </tr>
              </tbody>
            </table>
            <div class="payment-gateway-fee">
              + 4% Payment Gateway Fee Per Transaction
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="fixed">
            <table>
              <tbody>
                <tr class="bg-grey">
                  <td rowspan="2" class="bg-orange">BOOKINGS PER MONTH</td>
                  <td>FIRST 50 BOOKINGS</td>
                  <td>NEXT 50 BOOKINGS</td>
                  <td>THEREAFTER</td>
                </tr>
                <tr class="bg-light-grey">
                  <td>1-50</td>
                  <td>50-100</td>
                  <td>> 101</td>
                </tr>
                <tr class="bg-light-grey">
                  <td class="bg-orange">PRICE</td>
                  <td>8%</td>
                  <td>6%</td>
                  <td>4%</td>
                </tr>
              </tbody>
            </table>
            <div class="payment-gateway-fee">
              + 4% Payment Gateway Fee Per Transaction
            </div>
          </div>
        </div>

        <div class="outer-form-information"><!-- .outer-form-information -->
          <h3>YOUR INFORMATION</h3>
          <form name="registerform" id="registerform" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>"
              method="post">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Property Name</label>
                  <input type="text" name="user_login" id="user_login" class="form-control" placeholder="Enter Property Name">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6"><!-- .col- -->
                <div class="form-group">
                  <label>Property Type</label>
                  <input type="text" name="user_email" id="user_email" class="form-control" placeholder="Enter Property Type">
                  <input style="display:none" type="text" name="confirm_email" id="confirm_email" class="form-control" placeholder="Please leave this field empty">
                </div>
              </div><!-- /.col- -->
              <div class="col-sm-6"><!-- .col- -->
                <div class="form-group">
                  <label>Number Of Rooms</label>
                  <input type="number" min="0" max="1000000" class="form-control" placeholder="Enter number of rooms">  
                </div>
              </div><!-- /.col- -->
              <div class="col-sm-6"><!-- .col- -->
                <div class="form-group">
                  <label>Average booking per month</label>
                  <input type="number" min="0" max="1000000" class="form-control" placeholder="Avg booking per month">  
                </div>
              </div><!-- /.col- -->
              <div class="col-sm-6"><!-- .col- -->
                <div class="form-group">
                  <label>Website managed by</label>
                  <input type="text" class="form-control" placeholder="Who is manage your website?">  
                </div>
              </div><!-- /.col- -->
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Main Contact Name</label>
                  <input type="text" class="form-control" placeholder="Enter Main Contact Name">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6"><!-- .col- -->
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control input" value="" placeholder="Enter Property Email">
                </div>
              </div><!-- /.col- -->
              <div class="col-sm-3"><!-- .col- -->
                <div class="form-group">
                  <label>Office Phone</label>
                  <input type="text" class="form-control input" value="" placeholder="Enter Office Phone">
                </div>
              </div><!-- /.col- -->
              <div class="col-sm-3"><!-- .col- -->
                <div class="form-group">
                  <label>Whatsapp Number</label>
                  <input type="text" class="form-control input" value="" placeholder="Enter Whatsapp Phone">
                </div>
              </div><!-- /.col- -->
            </div>
            <div class="checkbox text-center">
              <label>
                <input type="hidden" name="redirect_to" value="<?php echo get_site_url(); ?>/login/?action=register&amp;success=1" />
                <input type="checkbox" required> I have read and agree to the <a href="#">Terms & Conditions</a>
              </label>
            </div>
            <div class="button-area text-center">
              <button type="submit" name="wp-submit" id="wp-submit" class="btn btn-default">REGISTER</button>
              <div class="already">
                Already have account? <a href="<?php echo get_site_url(); ?>/login/">Please login here</a>
              </div>
            </div>

          </form>
        </div><!-- /.outer-form-information -->
      </div><!-- /.outer-package-page -->
    </div><!-- end .container -->

  </div><!-- end .content -->
<?php get_footer(); ?>