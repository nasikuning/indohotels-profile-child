<?php /* Template Name: Product Template */ get_header('home'); ?>
<div id="wrapper" class="page">
    <div class="hero-image" style="background:url(<?php echo PROFILE_CHILD ?>/asset/img/new/product.jpg)">
      <div class="overlay"></div>
    </div>
    <div class="container singlepage">
      <div id="tabsinglepage" class="outer-tab-link">
        <ul class="nav nav-tabs" role="tablist">
          <li  role="presentation" class="active">
            <a href="#indohotels" aria-controls="indohotels" role="tab" data-toggle="tab">
              <span class="img-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-indohotels.svg" alt=""></span>
            </a>
          </li>
          <li role="presentation">
            <a href="#mytripbagus" aria-controls="mytripbagus" role="tab" data-toggle="tab">
              <span class="img-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-mytripbagus.svg" alt=""></span>
            </a>
          </li>
          <li role="presentation">
            <a href="#hotelsdirect" aria-controls="hotelsdirec" role="tab" data-toggle="tab">
              <span class="img-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-hotelsdirect.svg" alt=""></span>
            </a>
          </li>
          <!-- <li role="presentation">
            <a href="#starfusion" aria-controls="starfusion" role="tab" data-toggle="tab">
              <span class="img-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-starfusion.svg" alt=""></span>
            </a>
          </li> -->
          <li role="presentation">
            <a href="#booklogic" aria-controls="booklogic" role="tab" data-toggle="tab">
              <span class="img-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/icon-booklogic.svg" alt=""></span>
            </a>
          </li>
        </ul>
      </div> <!-- /.outer-tab-link -->
    </div><!-- end .container -->
    <div class="content-single">
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="indohotels">
          <div class="container">
            <h2>Indohotels.id</h2>
            <div class="row content-desc">
              <div class="col-sm-4">
                <img class="img-responsive" src="<?php echo PROFILE_CHILD ?>/asset/img/new/indohotels.png" alt="">
              </div>
              <div class="col-sm-8 right-text">
                <ul class="list-check">
                  <li>
                    <strong>We Are Local, Innovative & Efficient</strong>
                    <p>
                      <i>As a local company and having our head office in Yogyakarta, we are in a position to serve the hotel and it's guests best.</i>
                    </p>
                  </li>
                  <li>
                    <strong>Save Money With Our Commission System</strong>
                    <p>
                      <i>15% flat commission</i>
                    </p>
                  </li>
                  <li>
                    <strong>Fast & direct payment to hotel</strong>
                    <p>
                      <i>Payment to hotel will be made by bank transfer within 3 days of booking and without any hidden carges.</i>
                    </p>
                  </li>
                  <li>
                    <strong>No cancellations & refunds allowed</strong>
                    <p>
                      <i>Booking amendments can be made directly</i>
                    </p>
                  </li>
                </ul>
              </div>
            </div> <!-- /row -->
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Smart Features</h2>
              <div class="row">
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/responsive-extranet-system-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>Responsive extranet system</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/multiple-pricing-allotment-system-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>Multiple pricing & allotment system</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/welcome-thank-you-letters-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>welcome & thank you letters</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/daily-transaction-report-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>daily transaction report</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/multiple-user-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>multiple user</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/review-management-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>review management</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/competitor-setting-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>competitor setting</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/membership-discount-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>membership discount</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/mobile-discount-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>mobile discount</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/room-package-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>room package</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/promotional-code-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>promotional code</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/late-deals-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>late deals</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/bulk-discount-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>bulk discount</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
                <div class="col-xs-6 col-sm-4"> <!-- start looping -->
                  <div class="list-ota-content">
                    <div class="left-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/early-bird-icon.svg" alt="">
                    </div>
                    <div class="right-content">
                      <h4>Early bird</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end looping -->
              </div>
            </div>
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Hotel Partners</h2>
              <div class="clients-partners">
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/logo-eastparc.svg" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-adhiwangsa.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-cavinton-yogyakarta.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-grand-artos.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-megaland.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-plataran-hotel.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-royal-darmo.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-safira-magelang.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-sakanti-malioboro.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-syariah-hotel-solo.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-the-atrium.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/idn/logo-tjokrostyle-yogyakarta.png" alt="">
                  </div>
                </div>

                <!-- and more -->
                <p class="text-center">and more than 300 hotels</p>
                <div class="text-center">
                  <a class="btn-orange" href="https://www.indohotels.id/partnership-sign-up" target="_blank">SUBSCRIBE</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="mytripbagus">
          <div class="container">
            <h2>Mytripbagus.com</h2>
            <div class="row content-desc">
              <div class="col-sm-4">
                <img class="img-responsive" src="<?php echo PROFILE_CHILD ?>/asset/img/new/mytripbagus.png" alt="">
              </div>
              <div class="col-sm-8 right-text">
                <ul class="list-check">
                  <li>OTA Comparison for hotels & flights</li>
                </ul>
              </div>
            </div> <!-- /row -->
            <div class="text-center">
              <a class="btn-orange" href="https://mytripbagus.com/" target="_blank">SUBSCRIBE</a>
            </div>
          </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="hotelsdirect">
          <div class="container">
            <h2>Hotels Direct</h2>
            <div class="row content-desc">
            <div class="col-sm-4">
              <img class="img-responsive" src="<?php echo PROFILE_CHILD ?>/asset/img/new/hotels-direct.png" alt="">
            </div>
            <div class="col-sm-8 right-text">
              <ul class="list-check">
                <li>Build your own hotel website form our template at 20% off our regular price. (choice of 2 payment methods)
                  <ul>
                    <li>Free domain name, hosting & corporate email (1 year)</li>
                    <li>Free promotion materials & tent cards.</li>
                    <li>Free install our booking engine</li>
                  </ul>
                </li>
                <li>Install our booking engine to your existing hotel website. (choice of 2 payment methods)
                  <ul>
                    <li>Free promotion materials & tent cards.</li>
                  </ul>
                </li>
                <li>Free installation of our booking engine for your existing hotel website. (fixed monthly fee only)
                  <ul>
                    <li>Free promotion materials & tent cards.</li>
                  </ul>
                </li>
              </ul>
              <p><center><a href="#"><u><i>SEE PRICES</i></u></a></center></p>
            </div>
          </div> <!-- /row -->
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Payment Methods</h2>
              <div class="text-center">
                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
              </div>
              <h3>VARIABLE BOOKING COMMISSION</h3>
              <div class="outer-table">
                <table>
                  <tbody>
                    <tr class="bg-grey">
                      <td rowspan="2" class="bg-grey">BOOKINGS PER MONTH</td>
                      <td>FIRST 50 BOOKINGS</td>
                      <td>NEXT 50 BOOKINGS</td>
                      <td>THEREAFTER</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td>1-50</td>
                      <td>50-100</td>
                      <td>> 101</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">PRICE</td>
                      <td>8%</td>
                      <td>6%</td>
                      <td>4%</td>
                    </tr>
                  </tbody>
                </table>
                <table class="payment-gateway-fee">
                  <tr>
                    <td>+ 4% Payment Gateway Fee Per Transaction</td>
                  </tr>
                </table>
              </div>
              <h3>FIXED MONTHLY FEE</h3>
              <div class="outer-table">
                <table>
                  <tbody>
                    <tr>
                      <td colspan="7" class="text-center">MONTHLY FEES (AFTER 50% DISCOUNT)</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">HOTEL ROOMS</td>
                      <td>< 100</td>
                      <td>101 - 150</td>
                      <td>151 - 200</td>
                      <td>201 - 250</td>
                      <td>251 - 300</td>
                      <td>> 301</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">PRICE</td>
                      <td>IDR 990,000</td>
                      <td>IDR 1,490,000</td>
                      <td>IDR 1,990,000</td>
                      <td>IDR 2,490,000</td>
                      <td>IDR 2,990,000</td>
                      <td>IDR 3,990,000</td>
                    </tr>
                  </tbody>
                </table>
                <table class="payment-gateway-fee">
                  <tr>
                    <td>+ 4% Payment Gateway Fee Per Transaction</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Rate Comparison</h2>
              <div class="text-center">
                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
              </div>
              <div class="outer-table">
                <table>
                  <tbody>
                    <tr class="bg-orange">
                      <td class="text-center">BOOKINGS PER MONTH</td>
                      <td class="text-center">1 - 50</td>
                      <td class="text-center">51 - 100</td>
                      <td class="text-center">> 101</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">HOTEL DIRECT</td>
                      <td>12%</td>
                      <td>10%</td>
                      <td>8%</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">INDOHOTELS.ID</td>
                      <td>15%</td>
                      <td>15%</td>
                      <td>15%</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">COMPETITOR I</td>
                      <td>17%</td>
                      <td>17%</td>
                      <td>17%</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td class="bg-grey">COMPETITOR II</td>
                      <td>18+2%</td>
                      <td>18+2%</td>
                      <td>18+2%</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Hotel Partners</h2>
              <div class="clients-partners">
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/logo-eastparc.svg" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-gowongan-inn.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-grand-sarila.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-joglo-mandapa.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-lpp.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-new-saphir.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-prima-sr.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-queen-of-the-south.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-sky-hotel.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-the-amrani.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-wisma-aji.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/hoteldirect/logo-yellow-star.png" alt="">
                  </div>
                </div>

                <!-- and more -->
                <p class="text-center">and more than 300 hotels</p>
                <div class="text-center">
                  <a class="btn-orange" href="<?php echo get_site_url(); ?>/login/?action=register" target="_blank">SUBSCRIBE</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div role="tabpanel" class="tab-pane" id="starfusion">
          <div class="container">
            <h2>Starfusion</h2>
            <div class="row content-desc">
              <div class="col-sm-4">
                <img class="img-responsive" src="<?php echo PROFILE_CHILD ?>/asset/img/new/starfusion.png" alt="">
              </div>
              <div class="col-md-8 right-text">
                <ul class="list-check">
                  <li>Room reservations</li>
                  <li>Mobile reservations</li>
                  <li>Room Operations</li>
                  <li>Property Management System</li>
                  <li>Auto Prompt New Products</li>
                  <li>Auto Prompt Promotions</li>
                  <li>E-Services</li>
                  <li>Instant Registration of points</li>
                  <li>Real Time Feedback</li>
                </ul>
              </div>
            </div> 
          </div>
          <div class="content-white-single">
            <div class="container">
              <h3>MONTHLY FEES (AFTER 50% DISCOUNT)</h3>
              <div class="outer-table">
                <table>
                  <tbody>
                    <tr class="bg-orange">
                      <td class="text-center">OTA / ROOMS</td>
                      <td class="text-center">80 ROOMS</td>
                      <td class="text-center">81-160 / ROOMS</td>
                      <td class="text-center">161-240 / ROOMS</td>
                      <td class="text-center">241-320 / ROOMS</td>
                      <td class="text-center">321-480 / ROOMS</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td>6 OTA</td>
                      <td>IDR 790,000</td>
                      <td>IDR 990,000</td>
                      <td>IDR 1,240,000</td>
                      <td>IDR 1,540,000</td>
                      <td>IDR 2,040,000</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td>7 - 12 OTA</td>
                      <td>IDR 990,000</td>
                      <td>IDR 1,190,000</td>
                      <td>IDR 1,440,000</td>
                      <td>IDR 1,740,000</td>
                      <td>IDR 2,240,000</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td>13 - 18 OTA</td>
                      <td>IDR 1,240,000</td>
                      <td>IDR 1,440,000</td>
                      <td>IDR 1,690,000</td>
                      <td>IDR 1,990,000</td>
                      <td>IDR 2,490,000</td>
                    </tr>
                    <tr class="bg-light-grey">
                      <td>19 - 30 OTA</td>
                      <td>IDR 1,640,000</td>
                      <td>IDR 1,840,000</td>
                      <td>IDR 2,090,000</td>
                      <td>IDR 2,390,000</td>
                      <td>IDR 2,890,000</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="text-center">
                <a class="btn-orange" href="https://www.starfusion.com.sg/contact" target="_blank">SUBSCRIBE</a>
              </div>
            </div>
          </div>
        </div> -->
        <div role="tabpanel" class="tab-pane" id="booklogic">
          <div class="container">
            <h2>BookLogic</h2>
            <div class="row content-desc">
            <div class="col-sm-4">
              <img class="img-responsive" src="<?php echo PROFILE_CHILD ?>/asset/img/new/booklogic.png" alt="">
            </div>
            <div class="col-sm-8 right-text">
              <ul class="list-check">
                <li>Ensure price parity to all OTAs with a single entry.</li>
                <li>Shared availability between OTAs.</li>
                <li>Save cost on manpower. Automatic & efficient operation. No more human errors.</li>
                <li>Compatible to many property management systems.</li>
                <li>Extensive connection to leading OTAs.</li>
                <li>Over 10.000 customers.</li>
              </ul>
            </div>
          </div> <!-- /row -->
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Some Of Our OTA Connections</h2>
              <div class="clients-partners">
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/logo-indohotels.svg" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-agoda.svg" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-booking.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-expedia.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-pegipegi.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-rakuten.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-skyscanner.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-tiket.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-traveloka.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-trip-advisor.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-trivago.png" alt="">
                  </div>
                </div>
                <div class="list">
                  <div class="outer-list">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/client/booklogic/logo-wego.png" alt="">
                  </div>
                </div>

                <!-- and more -->
                <p class="text-center">and more than 120 OTA's</p>

              </div>
            </div>
          </div>
          <div class="content-white-single">
            <div class="container">
              <h2>Some Of Our PMS Connections</h2>
              <div class="clients-partners">
                <ul>
                  <li>Adobe Booking</li>
                  <li>Buzzotel</li>
                  <li>Be Housing</li>
                  <li>Callista</li>
                  <li>Centre Point System</li>
                  <li>Champagne</li>
                  <li>Fidelio</li>
                  <li>GAEA</li>
                  <li>GHM</li>
                  <li>GuestCentrix</li>
                  <li>GuestPoint</li>
                  <li>Hirum (Travelgate)</li>
                  <li>Maxial</li>
                  <li>Microgenn</li>
                  <li>MotelMate</li>
                  <li>Openhotelier</li>
                  <li>Opera</li>
                  <li>Power Brain</li>
                  <li>Room Booker Online</li>
                  <li>Room Traker</li>
                  <li>Room Soft</li>
                  <li>RMS</li>
                  <li>Satin</li>
                  <li>Seekom</li>
                </ul>
                <p class="text-center">and more than 50 PMS</p>
              </div>
              <div class="text-center">
                <a class="btn-orange" href="https://www.booklogic.net/for-hotels/channel-management/" target="_blank">SUBSCRIBE</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div><!-- end .content -->



<?php get_footer(); ?>