<?php /* Template Name: Service Template */ get_header('member');

?>
<div id="page-wrapper">
    <div class="container-fluid">
            <div class="row bg-title">
				<div class="col-md-12">
	  					<h2 class="page-title" style="font-weight: bold;">
	  						<i class="fa fa-home"></i> <?php the_title(); ?>
	  					</h2>
				</div>
			</div>
        <div class="panel panel-info">
            <div class="panel-body">
                <?php echo do_shortcode('[idh_service]'); ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer('member'); ?>