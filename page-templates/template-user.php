<?php /* Template Name: User Template */ get_header('member');

global $current_user;
wp_get_current_user();

// require_once( ABSPATH . WPINC . '/registration.php' );
        // print_r($_POST);
        // die();
if ( !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

	/* Update user password */
	if ( !empty($_POST['current_pass']) && !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {

		if ( !wp_check_password( $_POST['current_pass'], $current_user->user_pass, $current_user->ID) ) {

            $error = 'Your current password does not match. Please retry.';
            
		} elseif ( $_POST['pass1'] != $_POST['pass2'] ) {

            $error = 'The passwords do not match. Please retry.';
            
		} elseif ( strlen($_POST['pass1']) < 4 ) {

            $error = 'A bit short as a password, don\'t you thing?';
            
		} elseif ( false !== strpos( wp_unslash($_POST['pass1']), "\\" ) ) {

            $error = 'Password may not contain the character "\\" (backslash).';
            
		} else {

			$error = wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );

			if ( !is_int($error) ) {

                $error = 'An error occurred while updating your profile. Please retry.';
                
			} else {
                
				$error = false;
			}
        }

		if ( empty($error) ) {

            do_action('edit_user_profile_update', $current_user->ID);

            wp_redirect( site_url('/profile/') . '?success=1' );
            
			exit;
		}
    }

    // Save extra user profile
        save_extra_user_profile_fields( $current_user->ID );
} 

?>

<main id="main" class="site-main wrapper" role="main">
    <div class="container-fluid main-column">

        <?php while ( have_posts() ) : the_post(); ?>

        <article id="page-<?php the_ID(); ?>" class="meta-box hentry">
            <div class="post-content cf">

                <?php if ( !empty($_GET['success']) ): ?>
                <div class="message-box message-success">
                    <span class="icon-thumbs-up"></span>
                    Profile updated successfully!
                </div>
                <?php endif; ?>

                <?php if ( !empty($error) ): ?>
                <div class="message-box message-error">
                    <span class="icon-thumbs-up"></span>
                    <?php echo $error; ?>
                </div>
                <?php endif; ?>

                <div class="entry-header">
                    <h4 class="entry-title">Welcome,
                        <span class="userColor">
                            <?php echo esc_html($current_user->display_name); ?>
                        </span>
                    </h4>
                </div>

                <div class="entry-content">
                    <p>Pretty empty over here. Don’t worry it will fill up over time.</p>

                    <hr>
                </div>
                <!-- .entry-content -->

                <h2>Change password</h2>
                <p>You may change your password if you are so inclined.</p>

                <form method="post" id="adduser" action="<?php echo get_site_url(); ?>/profile/">
                    <div class="form-group row">
                        <label for="current_pass" class="col-sm-2 col-form-label">Current Password</label>
                        <div class="col-sm-10">
                        <input type="password" class="form-control" id="current_pass" name="current_pass" placeholder="Current Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="current_pass" class="col-sm-2 col-form-label">New Password</label>
                        <div class="col-sm-10">
                        <input type="password" class="form-control" id="pass1" name="pass1" placeholder="New Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="current_pass" class="col-sm-2 col-form-label">Confirm Password</label>
                        <div class="col-sm-10">
                        <input type="password" class="form-control" id="pass2" name="pass2" placeholder="Confirm Password">
                        </div>
                    </div>

                    <?php

                    // action hook for plugin and extra fields
                    do_action('edit_user_profile', $current_user);

                    ?>
                        <p class="form-submit">
                            <input name="updateuser" type="submit" id="updateuser" class="submit button btn" value="Update profile">
                            <input name="action" type="hidden" id="action" value="update-user">
                        </p>
                </form>

            </div>
        </article>

        <?php endwhile; ?>

    </div>
    <!-- .main-column -->

</main>
<!-- #main -->

<?php get_footer('member'); ?>