<?php get_header(); ?>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<br class="clear">

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h3 class="title text-center"><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h3>

			</article>
			<!-- /article -->

		<?php endif; ?>
<?php get_footer(); ?>