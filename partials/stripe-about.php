
    <div class="content-single">
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="whoweare">
          <div class="container">
            <h2><?php the_title(); ?></h2>
            <div class="content-desc">
                <?php the_content(); ?>
            </div> <!-- content-desc -->
          </div>
        </div>
      </div>
    </div>