<?php
$maps = get_sub_field('maps');
print_r($maps);
die();
if( !empty($maps) ):
?>
<div class="acf-map">
    <div class="marker" data-lat="<?php echo $maps['lat']; ?>" data-lng="<?php echo $maps['lng']; ?>"></div>
</div>
<?php endif; ?>