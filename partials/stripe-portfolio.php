    <div class="container">
      <div class="content-single">
        <div class="tab-content">
          <div role="tabpanel" id="portfolio">


            <h2><?php the_sub_field('title'); ?></h2>
            <div class="content-desc">
              <div class="content-portfolio">
                <div class="outer-random-portfolio">
                <?php $posts = get_sub_field('content');
                  foreach ($posts as $post) : 
                ?>
                <div class="portfolio-list">
                    <?php 
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail();
                    } 
                    ?>
                    <div class="overlay">
                      <div class="desc">
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                      </div>
                    </div>
                  </div>
                <?php endforeach ?>
                <?php wp_reset_postdata(); ?>
                  <div class="clearfix"></div>
                </div>
              </div><!-- end -->
            </div>
          </div>
        </div>
      </div>
    </div><!-- end .container -->