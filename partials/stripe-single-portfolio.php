<?php $content= get_field('content'); 
// print_r($content);
// die(); ?>
<div class="outer-grid-portfolio">
    <div class="row-one">
    <div class="left">
        <div class="top">
        <div class="item">
            <div class="outer-item">
            <div class="overlay">
                <div class="outer-content">
                <div class="name"><?php echo $content[0]['title']; ?></div>
                <div class="button"><a href="<?php echo $content[0]['demo_url']; ?>">see website</a></div>
                </div>
            </div>
            <img src="<?php echo $content[0]['thumbnail']['url']; ?>" alt="<?php echo $content[0]['title']; ?>">
            </div>
        </div>
        </div>
        <div class="bottom">
        <div class="item">
            <div class="outer-item">
            <div class="overlay">
                <div class="outer-content">
                <div class="name"><?php echo $content[1]['title']; ?></div>
                <div class="button"><a href="<?php echo $content[1]['demo_url']; ?>">see website</a></div>
                </div>
            </div>
            <img src="<?php echo $content[1]['thumbnail']['url']; ?>" alt="<?php echo $content[1]['title']; ?>">
            </div>
        </div>
        </div>
    </div>
    <div class="right">
        <div class="top">
        <div class="item">
            <div class="outer-item">
            <div class="overlay">
                <div class="outer-content">
                <div class="name"><?php echo $content[2]['title']; ?></div>
                <div class="button"><a href="<?php echo $content[2]['demo_url']; ?>">see website</a></div>
                </div>
            </div>
            <img src="<?php echo $content[2]['thumbnail']['url']; ?>" alt="<?php echo $content[2]['title']; ?>">
            </div>
        </div>
        </div>
        <div class="bottom">
        <div class="item1">
            <div class="outer-item">
            <div class="overlay">
                <div class="outer-content">
                <div class="name"><?php echo $content[3]['title']; ?></div>
                <div class="button"><a href="<?php echo $content[3]['demo_url']; ?>">see website</a></div>
                </div>
            </div>
            <img src="<?php echo $content[3]['thumbnail']['url']; ?>" alt="<?php echo $content[3]['title']; ?>">
            </div>
        </div>
        <div class="item2">
            <div class="outer-item">
            <div class="overlay">
                <div class="outer-content">
                <div class="name"><?php echo $content[4]['title']; ?></div>
                <div class="button"><a href="<?php echo $content[4]['demo_url']; ?>">see website</a></div>
                </div>
            </div>
            <img src="<?php echo $content[4]['thumbnail']['url']; ?>" alt="<?php echo $content[4]['title']; ?>">
            </div>
        </div>
        </div>
    </div>
    <div class="clearfix"></div>
    </div><!-- /.row -->
</div>