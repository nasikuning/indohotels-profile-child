<?php 
    $images = get_field('sliders'); 
    if($images) {
        foreach ($images as $image) {
            echo '<div class="hero-image" style="background-image: url(\''.$image['url'].'\')"><div class="overlay"></div></div>';
        }
    }
?>