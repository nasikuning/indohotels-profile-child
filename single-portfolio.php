<?php get_header(); ?>

    <div id="wrapper" class="page">
    <?php

    if (have_posts()) : while (have_posts()) : the_post();

        get_template_part('partials/stripe', 'slider');
        
        $our_title = get_the_title( get_option('page_for_posts', true) );


        ?>
        
    <div class="container">
      <div class="content-single">
        <div class="tab-content">
          <div role="tabpanel" id="portfolio">
            <h2>Portfolio - <?php the_title(); ?></h2>
            <div class="content-desc">
                <div class="text-center">
                  <p>
                    </p>
                </div>

                <?php get_template_part('partials/stripe', 'single-portfolio'); ?>

              </div>
          </div>

        </div>
      </div>
    </div><!-- end .container -->

    <?php endwhile; endif; // close the WordPress loop ?>

  </div><!-- end .content -->
  <?php get_footer(); ?>