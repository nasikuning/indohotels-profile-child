<?php /* Template Name: Index Template */ get_header('property'); ?>
<main role="main">
    <section class="booking-section">
        <div class="container">
            <div class="booking-box">
                <?php do_shortcode("[booking_engine]"); ?>
            </div>
        </div>
    </section>
    <?php top_deals(); ?>

    <section id="welcome-hotel">
        <div class="container">
            <div class="box-bg">
			<?php
				$group_values = rwmb_meta( 'standard' );
				$field_title = 'title-section';
				$field_content = 'content-section';

				if ( ! empty( $group_values ) ) {
					foreach ( $group_values as $group_value ) {
						$title_section = isset( $group_value[$field_title] ) ? $group_value[$field_title] : '';
						$content_section = isset( $group_value[$field_content] ) ? $group_value[$field_content] : '';

						echo '<h2>'.$title_section.'</h2>';
						echo '<span class="line"></span>';
						echo '<p>' . $content_section . '</p>';
					}
				}
			?>
            </div>
        </div>
    </section>





    <!-- section -->
    <?php if (ot_get_option('krs_room_actived') != 'no') : ?>
    <section id="ros-in" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(<?php echo ot_get_option('krs_background_text1'); ?>);">
        <div class="container">


            <?php 
			$args = array(
                'post_type'=> ot_get_option('krs_section_2'),                
                'posts_per_page' => 6,                
            );

            $krs_query = new WP_Query( $args );
            
            $count = $krs_query->post_count;

            if(($count == 2) || ($count == 4)) {
                $col = 'col-md-6';                
            } else {
                $col = 'col-md-4';                
            }

            if ($krs_query->have_posts()): ?>
                <div class="box-bg" <?php if($col == 'col-md-4') { echo 'style="width: 70%";'; } ?>>
                <h2><?php echo ot_get_option('krs_headline_section2');?></h2>
                    <div class="row">
                        <?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
                        <div class="<?php echo $col ?>">

                            <div class="box-room">
                                <div class="thumb">
                                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('medium'); // Declare pixel size you need inside the array ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
                                <h4><?php the_title(); ?></h4>
                            </div>       
                        </div>
                    <?php endwhile; ?>
                    </div>
                    <?php endif; ?>

                </div> <!-- /box-bg -->
        </div>
    </section>
    <?php endif; ?>
    <!-- /section -->
    <?php if (ot_get_option('krs_section3_actived') != 'no') : ?>
    <section class="hotel-property text-center">
        <div class="container">

            <?php
		$args = array(
			'post_type' => 'property',
			);
		query_posts($args);
		if (have_posts()) : ?>
                <h3><?php echo ot_get_option('krs_headline_section3');?></h3>
                <span class="line"></span>
                <div class="row">
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="item col-md-4">
                        <div class="thumbnails">
                            <?php if ( has_post_thumbnail() ) : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <img class="img-responsive" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
                            </a>
                            <?php endif; ?>
                        </div>
                        <h4>
                            <?php echo get_the_title(); ?>
                        </h4>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- gallery -->
    <section id="image-popups" class=" text-center">
        <?php
		$args = array(
			'post_type' => 'gallery', 
			'phototype'  => 'home',
			);
		query_posts($args);
		if (have_posts()) : ?>
            <h3><?php _e('IMAGE GALLERY', karisma_text_domain); ?></h3>
            <span class="line"></span>
            <div class="box-home-grid">
                <?php while (have_posts()) : the_post(); ?>
                <div class="item col-md-4">
                    <div class="thumbnails">
                        <?php if ( has_post_thumbnail() ) : ?>
                        <a href="<?php the_post_thumbnail_url('gallery-slide'); ?>" title="<?php the_title_attribute(); ?>">
                            <img class="image-popups" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
                            <div class="overlay"><span><?php the_title_attribute(); ?></span></div>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            <div class="clearfix"></div>
    </section>

    <!-- end gallery -->
    <?php footer_slide(); ?>
</main>
<?php get_footer(); ?>